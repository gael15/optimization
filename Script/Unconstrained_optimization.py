#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 22 15:10:08 2021

@author: guetin gael
"""
# =============================================================================
# Librairies
# =============================================================================
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import least_squares

# =============================================================================
# Gradient descent
# =============================================================================

def f2plot (x1,x2):
    return 5*x1**2 + (x2**2)/2 - 3*(x1 + x2)
    
def f2(x):
    return 5*x[0,0]**2 + (x[1,0]**2)/2 - 3*(x[0,0]+ x[1,0]) 

def gradf2 (x):
    grad1 = 10 *x[0,0] - 3
    
    grad2 = x[1,0] - 3
    
    grad = np.array([[grad1],[grad2]])
    
    return grad
    
# =============================================================================
# Constant step size 
# =============================================================================
def constStepGD (F, x0, gradF, tol = 10**(-6), Niter =100):
    
    #Initialisation
    i=0
    rho = 0.01
    t0 = rho
    xSt = []
    xSt_2 =[]
    xi = x0
            
    #Compute d0
    d0 = -gradF(xi)
    
    #Compute x1
    xi_1 = x0 + t0*d0
    
    #Loop
    for i in range (Niter):
        
        d1 = -gradF(xi_1)
        
        t1 = t0
        
        xi_1 = xi + t1*d1
        
        xSt.append(xi_1[0,0]) 
        
        xSt_2.append(xi_1[1,0]) 
        
        if (np.linalg.norm(gradF(xi_1)) < tol ):
            
            print("xSt = ",xSt)
            print("xSt_2=",xSt_2)
            
            print("The algorithm converged good job")
            
            return xSt,xSt_2
        
        else :
            
          xi=xi_1
          
          xSt.append(xi_1[0,0]) 
          
          xSt_2.append(xi_1[1,0]) 

    print (" Error , the algorithm has not converged after ", Niter , " iterations ")
    print("xSt = ",xSt)
    print("xSt_2=",xSt_2)
    
    return xSt,xSt_2

# =============================================================================
# Optimal step size 
# =============================================================================
def optStepGD (F, x0, gradF, A, tol = 10**(-6), Niter =100):
    
    #Initialisation
    i=0
    xSt = []
    xSt_2 =[]
    
    xi = x0
            
    #Compute d0
    d0 = -gradF(xi)
    
    #Compute t0
    
    t0 = -((np.transpose(gradF(xi))@ d0)/(np.transpose(d0)@ A @d0))
    
    #Compute x1
    
    xi_1 = xi + t0*d0
    
    #Loop
    
    for i in range (Niter):
        
        d1 = -gradF(xi_1)
        
        t1 = -((np.transpose(gradF(xi))@ d1)/(np.transpose(d1)@ A @d1))
        
        xi_1 = xi + t1*d1
        
        xSt.append(xi_1[0,0]) 
        
        xSt_2.append(xi_1[1,0]) 
     
        
        if (np.linalg.norm(gradF(xi_1)) < tol ):
            
            print("xSt = ",xSt)
            print("xSt_2=",xSt_2)
            
            print("The algorithm converged good job")
            
            return xSt,xSt_2
        
        else :
            
          xi=xi_1
          
    print (" Error , the algorithm has not converged after ", Niter , " iterations ")
    print("xSt = ",xSt)
    print("xSt_2=",xSt_2)

    return xSt,xSt_2

# =============================================================================
# Conjugate gradient descent: 
# =============================================================================

def conjGD(F, x0, gradF, A, tol = 10**(-6), Niter =100):
    
    d0 = - gradF(x0)
    
    xSt =[]
    
    xSt_2= []
    
    xSt.append(x0[0,0])
    
    xSt_2.append(x0[1,0])
    
    for i in range (Niter):
        
        if np.linalg.norm(gradF(x0)) < tol:
            
            print("Nous avons convergé")
            
        else:
            
            t0 = - (gradF(x0).T @ d0)/(d0.T @A@ d0)
            
            x1 = x0 + t0*d0
            
            Betha = d0.T@A@gradF(x1)/(d0.T @A@ d0)
            
            d0 = - gradF(x1) + Betha*d0
            
            x0 = x1
            
            xSt.append(x0[0,0])
            
            xSt_2.append(x0[1,0])
            
    return xSt,xSt_2

def trian_sensors(x,y):
    
    x1 =-1
    x2 =1
    x3 =0
    
    y1 =0
    y2 =0
    y3 =1
    
    d1 =1
    d2 =1
    d3 =2
    
    #Equation of 3 circles
    
    Eq_1 = (x1 - x)**2 + (y1 - y)**2 - d1**2
    
    Eq_2 = (x2 - x)**2 + (y2 - y)**2 - d2**2
    
    Eq_3 = (x3 - x)**2 + (y3 - y)**2 - d3**2

    #Least squares solution
    
    A = np.array([[2*(x1-x2), 2*(y1-y2)],[2*(x1-x3), 2*(y1-y3)],[2*(x2-x3), 2*(y2-y3)]])
    
    b = np.array([[-d1**2 + d2**2 + x1**2 - x2**2 + y1**2 - y2**2],[-d1**2 + d3**2 + x1**2 - x3**2 + y1**2 - y3**2],[-d2**2 + d3**2 + x2**2 - x3**2 + y2**2 - y3**2]])
    
    x_star = np.linalg.pinv(A)@ b
    
    return Eq_1,Eq_2,Eq_3,x_star

def func(X):
       
        x = X[0]
        y = X[1]
        
        x1 =-1
        x2 =1
        x3 =0
        
        y1 =0
        y2 =0
        y3 =1
        
        d1 =1
        d2 =1
        d3 =2
        
        #Equation des 3 cercles
        
        Eq_1 = (x1 - x)**2 + (y1 - y)**2 - d1**2
        
        Eq_2 = (x2 - x)**2 + (y2 - y)**2 - d2**2
        
        Eq_3 = (x3 - x)**2 + (y3 - y)**2 - d3**2
        
        Eq = (Eq_1**2 + Eq_2**2 + Eq_3**2).flatten()
        
        return Eq

if __name__ == '__main__' :
    
    a = np.linspace(-3.2,3.2,150)
    
    b = np.linspace(-3.2,3.2,150)
    
    x, y = np.meshgrid( a, b ) 
    
    x1,x2 = np.meshgrid(np.linspace(-8,8,201),np.linspace(-8,8,201))
    
    data1 = np.array([[-6],[6]])#Data 1
    
    data2 = np.array([[0],[0]])#Data 2
    
    data3 = np.array([[-2],[-7]])#Data 3
    
    A = np.array([[10, 0],[0, 1]])
    
    b = np.array([[3],[3]])
    
    x_star = np.linalg.pinv(A)@ b
    
# =============================================================================
#Fonc : Conststep
# =============================================================================
    
    xMin,xMin_1 = constStepGD (f2,data1,gradf2)
    
    xMin_2,xMin_3 = constStepGD (f2,data2,gradf2)
    
    xMin_4,xMin_5 = constStepGD (f2,data3,gradf2)
# =============================================================================
#Fonc : optimalstep
# =============================================================================
    xMin_6,xMin_7 = optStepGD (f2,data1,gradf2,A)
    
    xMin_8,xMin_9 = optStepGD (f2,data2,gradf2,A)
    
    xMin_10,xMin_11 = optStepGD (f2,data3,gradf2,A)
   
# =============================================================================
#Fonc : conjug grad
# =============================================================================
    xMin_12,xMin_13 = conjGD (f2,data1,gradf2,A)
    
    xMin_14,xMin_15 = conjGD (f2,data2,gradf2,A)
    
    xMin_16,xMin_17 = conjGD (f2,data3,gradf2,A)
    
# =============================================================================
#Sensors Triangulation 
# =============================================================================
    
    Eq_1,Eq_2,Eq_3,x_st = trian_sensors(x,y)
    
    data = [0,0]
    
    solution = least_squares(func,data)
    
# =============================================================================
# Plot: Conststep
# =============================================================================
    plt.figure(1)
    Z = f2plot(x1,x2)
    plt.contour(x1,x2,Z,np.linspace(0,1000,50))
    plt.plot(xMin,xMin_1,'o-',c='g')
    plt.plot(xMin_2,xMin_3,'o-',c='b')
    plt.plot(xMin_4,xMin_5,'o-',c='r')
    plt.colorbar()
    plt.title( 'Constant step Size' ) 
    plt.show()
    
# # =============================================================================
# # Plot : OptimalStep
# # =============================================================================
    plt.figure(2)
    Z = f2plot(x1,x2)
    plt.contour(x1,x2,Z,np.linspace(0,1000,50))
    plt.plot(xMin_6,xMin_7,'o-',c='g')
    plt.plot(xMin_8,xMin_9,'o-',c='b')
    plt.plot(xMin_10,xMin_11,'o-',c='r')
    plt.colorbar()
    plt.title( 'Optimal step Size' ) 
    plt.show()
    
# =============================================================================
# Plot : Gradient descent
# =============================================================================
    plt.figure(3)
    Z = f2plot(x1,x2)
    plt.contour(x1,x2,Z,np.linspace(0,1000,50))
    plt.plot(xMin_12,xMin_13,'o-',c='g')
    plt.plot(xMin_14,xMin_15,'o-',c='b')
    plt.plot(xMin_16,xMin_17,'o-',c='r')
    plt.title( 'Conjugate gradient Descent' ) 
    plt.colorbar()
    plt.show()
    
# =============================================================================
# Plot circles
# =============================================================================
 
    plt.figure(4)
    
    plt.contour( x , y , Eq_1,[0], colors=['r'] ) 
    
    plt.contour( x , y , Eq_2,[0], colors=['b'] ) 
    
    plt.contour( x , y , Eq_3,[0], colors=['g'] ) 
    
    plt.plot(solution.x[0],solution.x[1],"-o",c='k')
    
    plt.title( 'Landmark circles' ) 
    
    plt.show() 
    
    
